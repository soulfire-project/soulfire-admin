/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @factory ${NAME}
 */

angular.module("SoulfireAdmin.Factories").factory("${NAME}", function ${NAME}()
{
    "use strict";
    
    return this;
});