/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @service ${NAME}
 */

angular.module("SoulfireAdmin.Services").factory("${NAME}", function ${NAME}()
{
    "use strict";
    
    return this;
});