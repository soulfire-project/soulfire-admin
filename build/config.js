/* jshint strict: false */
module.exports = new function() {
	this.root_dest = "./dist/resources/admin";
	this.app_dest = "./dist";

	this.app = {
	    dest: this.root_dest + "/application",
        watchlist: "app/src/**/*.js"
	};

	this.styles = {
		dest: this.root_dest + "/assets/styles",
		filename: "app.css",
		watchlist: [
			"app/assets/sass/*.scss",
			"app/assets/sass/includes/*.scss"
		]
	};

	this.images = {
		dest: this.root_dest + "/assets/images",
		watchlist: "app/assets/images/**/*"
	};

	this.html = {
		dest: this.app_dest,
        del: this.app_dest + "/.html",
		watchlist: "app/*.html"
	};

    this.layout = {
        dest: this.root_dest + "/layout",
        watchlist: "app/layout/**/*.html"
    };

	this.vendors = {
		dest: this.root_dest + "/vendor"
	};
};