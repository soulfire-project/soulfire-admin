/* jshint strict: false */
module.exports = {
	vendors: [
		"app/vendor/**/*.min.js",
		"app/vendor/**/*.min.css"
	],

	app: [
		"app/src/**/*.js"
	],

	styles: [
		"app/assets/sass/**/*.scss"
	],

	images: [
		"app/assets/images/**/*"
	],

	html: [
		"app/*.html"
	],

	layout: [
		"app/layout/**/*.html"
	]
};