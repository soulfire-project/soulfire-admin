/* jshint strict: false */
var gulp = require("gulp");
var concat = require("gulp-concat");
var del = require("del");
var sass = require("gulp-sass");

var config = require("./build/config.js");
var files = require("./build/files.js");

// Clear task
gulp.task("clear", function() {
	del(config.root_dest);
});

// Vendors task
gulp.task("vendors", function() {
	del(config.vendors.dest, function() {
		gulp.src(files.vendors)
			.pipe(gulp.dest(config.vendors.dest));
	});
});

// Styles task
gulp.task("styles", function() {
	del(config.styles.dest, function() {
		gulp.src(files.styles)
			.pipe(sass().on("error", sass.logError))
			.pipe(concat(config.styles.filename))
			.pipe(gulp.dest(config.styles.dest));
	})
});

// Images task
gulp.task("images", function() {
	del(config.images.dest, function() {
		gulp.src(files.images)
			.pipe(gulp.dest(config.images.dest));
	})
});

// Application task
gulp.task("app", function() {
    del(config.app.dest, function() {
        gulp.src(files.app)
            .pipe(gulp.dest(config.app.dest));
    })
});

// Layout task
gulp.task("layout", function() {
    del(config.layout.dest, function() {
        gulp.src(files.layout)
            .pipe(gulp.dest(config.layout.dest));
    })
})

// Html task
gulp.task("html", function() {
	del(config.html.del, function() {
		gulp.src(files.html)
			.pipe(gulp.dest(config.html.dest));
	})
});

// Watch task
gulp.task("watch", ['default'], function() {
    gulp.watch(config.app.watchlist, ['app']);
	gulp.watch(config.styles.watchlist, ['styles']);
	gulp.watch(config.images.watchlist, ['images']);
    gulp.watch(config.layout.watchlist, ['layout']);
	gulp.watch(config.html.watchlist, ['html']);
});

// Default task & watch
gulp.task("default", [
	"vendors",
	"styles",
	"images",
    "app",
    "layout",
	"html"
]);