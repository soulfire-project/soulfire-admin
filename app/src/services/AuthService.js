/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @service AuthService
 */

angular.module("SoulfireAdmin.Services").factory("AuthService", function AuthService($http, $q)
{
    "use strict";

    this.isAuthenticated = false;
    this.userData = {};
    this.teste = false;

    var self = this;

    self.setGuestData = function setGuestData()
    {
        self.isAuthenticated = false;
        self.userData = {};
    };

    this.getAuthStatus = function getAuthStatus()
    {
        var deferred = $q.defer();

        $http.get($apiUrl.admin + "auth/status").success(function(response)
        {
            if(response.status === "ok")
            {
                self.isAuthenticated = true;
                self.userData = response.data;
            }
            else if(response.status === "guest")
            {
                self.setGuestData();
            }

            deferred.resolve();
        }).error(function()
        {
            self.setGuestData();
            deferred.reject();
        });

        return deferred.promise;
    };

    this.authenticate = function authenticate(request)
    {
        var deferred = $q.defer();

        $http.post($apiUrl.admin + "auth/login", request).success(function(response)
        {
            deferred.resolve(response);
        }).error(function()
        {
            deferred.reject("Failed to connect on API.");
        });

        return deferred.promise;
    };

    return self;
});