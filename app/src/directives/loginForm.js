/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @directive loginForm
 */

angular.module("SoulfireAdmin.Directives").directive("loginForm", function loginForm()
{
    "use strict";

    return {
        scope: {},
        templateUrl: $getPath.layouts + "/login-form.html"
    };
});