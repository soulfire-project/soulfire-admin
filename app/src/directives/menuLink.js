/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @directive menuLink
 */

angular.module("SoulfireAdmin.Directives").directive("menuLink", function menuLink()
{
    "use strict";

    return {
        scope: {
            section: "="
        },
        templateUrl: $getPath.layouts + "/menu-link.html",
        link: function($scope, $element)
        {
            var controller = $element.parent().controller();

            $scope.isSelected = function()
            {
                return controller.isSelected($scope.section);
            };

            $scope.focusSection = function()
            {
                controller.autoFocusContent = true;
            };
        }
    };
});