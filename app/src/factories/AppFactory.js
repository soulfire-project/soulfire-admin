/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @factory AppFactory
 */

angular.module("SoulfireAdmin.Factories").factory("AppFactory", function AppFactory($http, $q)
{
    "use strict";

    this.resultSuccess = function(response)
    {
        this.menus = response.menus;
    };

    this.getIndexData = function getIndexData()
    {
        $http.get($api_url.admin + "index-data").success(this.resultSuccess);
    };

    return this;
});