/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @factory MenuFactory
 */

angular.module("SoulfireAdmin.Factories").factory("MenuFactory", function MenuFactory($location, $rootScope)
{
    "use strict";

    $rootScope.$on('$locationChangeSuccess', onLocationChange);

    this.sections = [
        {
            name: "Dashboard",
            type: "heading",
            children: [
                {
                    name: "Home",
                    type: "link",
                    url: ""
                }
            ]
        },
        {
            name: "Sistema",
            type: "heading",
            children: [
                {
                    name: "Configurações",
                    type: "toggle",
                    pages: [
                        {
                            name: "Configurações Gerais",
                            type: "link",
                            url: ""
                        },
                        {
                            name: "Painel de Controle",
                            type: "link",
                            url: ""
                        }
                    ]
                }
            ]
        }
    ];

    this.selectSection = function(section)
    {
        this.openedSection = section;
    };

    this.toggleSelectSection = function(section)
    {
        this.openedSection = (this.openedSection == section ? null : section);
    };

    this.isSectionSelected = function(section)
    {
        return this.openedSection === section;
    };

    this.selectPage = function(section, page)
    {
        this.currentSection = section;
        this.currentPage = page;
    };

    this.isPageSelected = function(page)
    {
        return this.currentPage === page;
    };

    var $self = this;

    function onLocationChange()
    {
        var path = $location.path();
        var dashboardLink = {
            name: "Dashboard",
            url: "/",
            type: "link"
        };

        var matchPage = function(section, page)
        {
            if(path === page.url)
            {
                $self.selectSection(section);
                $self.selectPage(section, page);
            }
        };

        if(path === "/")
        {
            $self.selectSection(dashboardLink);
            $self.selectPage(dashboardLink, dashboardLink);
        }

        $self.sections.forEach(function(section)
        {
           section.children.forEach(function(childSection)
           {
               if(childSection.pages)
               {
                   childSection.pages.forEach(function(page)
                   {
                       matchPage(childSection, page);
                   });
               }
               else if(section.pages)
               {
                   section.pages.forEach(function(page)
                   {
                       matchPage(section, page);
                   });
               }
               else if(section.type === "link")
               {
                   matchPage(section, section);
               }
           });
        });
    }

    return this;
});