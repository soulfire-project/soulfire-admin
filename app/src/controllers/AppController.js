/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @controller AppController
 */

angular.module("SoulfireAdmin.Controllers").controller("AppController", function AppController($mdSidenav, $mdDialog, $timeout, $location, AuthService, AppFactory, MenuFactory)
{
    "use strict";

    //AppFactory.getIndexData();

    this.isOpen = isOpen;
    this.isSelected = isSelected;
    this.toggleOpen = toggleOpen;
    this.autoFocusContent = false;

    var mainContentArea = document.querySelector("[role='main']");

    function closeMenu()
    {
        $timeout(function()
        {
            $mdSidenav("left").close();
        });
    }

    function openMenu()
    {
        $timeout(function()
        {
            $mdSidenav("left").open();
        });
    }

    function openPage()
    {
        closeMenu();

        if(AppController.autoFocusContent === true)
        {
            focusMainContent();
            AppController.autoFocusContent = false;
        }
    }

    function focusMainContent($event)
    {
        if($event)
        {
            $event.preventDefault();
        }

        $timeout(function()
        {
            mainContentArea.focus();
        });
    }

    function isSelected(page)
    {
        return MenuFactory.isPageSelected(page);
    }

    function isSectionSelected(section)
    {
        var selected = false;
        var openedSection = MenuFactory.openedSection;

        if(openedSection === section)
        {
            selected = true;
        }
        else if(section.children)
        {
            section.children.forEach(function(childSection)
            {
                if(childSection === openedSection)
                {
                    selected = true;
                }
            });
        }

        return selected;
    }

    function isOpen(section)
    {
        return MenuFactory.isSectionSelected(section)
    }

    function toggleOpen(section)
    {
        return MenuFactory.toggleSelectSection(section);
    }

    function openLoginForm()
    {
        $mdDialog.show({
            templateUrl: $getPath.layouts + "/login-form.html",
            escapeToClose: false
        });
    }

    angular.extend(this, {
        content: "teste de conteúdo aqui",
        openMenu: openMenu,
        closeMenu: closeMenu,
        openPage: openPage,
        isSectionSelected: isSectionSelected,
        focusMainContent: focusMainContent,
        menu: MenuFactory,
        auth: AuthService,
        toggleSidenav: function(menuId)
        {
            $mdSidenav(menuId).toggle();
        }
    });

    AuthService.getAuthStatus().then(function()
    {
        if(AuthService.isAuthenticated === false)
        {
            //openLoginForm();
        }
    });
});