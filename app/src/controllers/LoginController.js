/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @controller LoginController
 */

angular.module("SoulfireAdmin.Controllers").controller("LoginController", function LoginController($mdDialog, AuthService)
{
    "use strict";

    this.request = {
        username: "",
        password: ""
    };

    this.login = function login()
    {
        AuthService.authenticate(this.request).then(function(response)
        {
            if(response.result === "validate")
            {
                alert("manja ae doido");
            }
            else if(response.result === "error")
            {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Erro')
                        .content('Login ou senha inválidos.')
                        .ariaLabel('Alert')
                        .ok('Fechar')
                );
            }
            else if(response.result === "success")
            {
                AuthService.getAuthStatus();
                $mdDialog.hide();
            }
        });
    };

    angular.extend(this, {});
});