/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @directive nospaceFilter
 */

angular.module("SoulfireAdmin.Filters").filter("nospace", function nospace()
{
    "use strict";

    return function(value)
    {
        return (!value) ? "" : value.replace(/ /g, "");
    };
});