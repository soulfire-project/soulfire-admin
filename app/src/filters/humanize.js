/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @directive humanize
 */

angular.module("SoulfireAdmin.Filters").filter("humanize", function humanize()
{
    "use strict";

    return function(value)
    {
        if(!value)
        {
            return "";
        }

        if(value.type == "directive")
        {
            return value.name.replace(/(A-Z)/g, function($1)
            {
                return "-" + $1.toLowerCase();
            });
        }

        return value.label || value.name;
    };
});