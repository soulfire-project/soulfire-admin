/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

angular.module("SoulfireAdmin").config(function RouterConfig($stateProvider, $urlRouterProvider)
{
   $stateProvider.state("dashboard", {
       url: "/",
       template: "aew jhow"
   }) ;

    //$urlRouterProvider.otherwise("/");
});