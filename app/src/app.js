/**
 * This file is part of the Soulfire package.
 *
 * (c) Erick Carvalho <erick.mcarvalho@live.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

var $getPath = {
    root: "resources/admin",
    layouts: "resources/admin/layout",
    views: "resources/admin/views"
};

var $apiUrl = {
    root: "./api/",
    app: "./api/app/",
    admin: "./api/app/admin/"
};

angular.module("SoulfireAdmin.Filters", []);
angular.module("SoulfireAdmin.Directives", []);
angular.module("SoulfireAdmin.Factories", []);
angular.module("SoulfireAdmin.Services", ['SoulfireAdmin.Factories']);
angular.module("SoulfireAdmin.Controllers", ['SoulfireAdmin.Services']);
angular.module("SoulfireAdmin", ['ngMaterial', 'ui.router', 'SoulfireAdmin.Controllers', 'SoulfireAdmin.Directives', 'SoulfireAdmin.Filters']);

angular.module("SoulfireAdmin").config(function AppConfig($mdThemingProvider)
{
    "use strict";

    $mdThemingProvider.definePalette("app-blue", $mdThemingProvider.extendPalette("blue", {
        "50": "#DCEFFF",
        "100": "#AAD1F9",
        "200": "#7BB8F5",
        "300": "#4C9EF1",
        "400": "#1C85ED",
        "500": "#106CC8",
        "600": "#0159A2",
        "700": "#025EE9",
        "800": "#014AB6",
        "900": "#013583",
        "contrastDefaultColor": "light",
        "contrastDarkColors": "50 100 200 A100",
        "contrastStrongLightColors": "300 400 A200 A400"
    }));

    $mdThemingProvider.definePalette("app-red", $mdThemingProvider.extendPalette("red", {
        "A100": "#DE3641"
    }));

    $mdThemingProvider.theme("default")
        .primaryPalette("app-blue")
        .accentPalette("app-red");
});